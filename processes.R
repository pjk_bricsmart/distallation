library(tidyverse)
library(lubridate)
library(caret)
library(corrplot)
library(randomForest)
library(janitor)
library(naniar)
library(ggplot2)
library(gridExtra)

#####     #####     #####     #####     #####

df <- readxl::read_xlsx('PS.xlsx') %>%
  mutate(Date = lubridate::dmy(Date)) %>%
  select(-Hour) %>%
  data.frame()

#####     #####     #####     #####     #####

processes <- readxl::read_xlsx('processes.xlsx', na = 'NA') %>%
  janitor::clean_names()

# gg_miss_var(processes)

processes_df <- processes %>%
  select(
    -vazao_de_agua_fria_para_c_415_topo_da_e_406,
    -nivel_de_fenol_na_base_da_e_404
  )

# gg_miss_var(processes_df)

processes_df <- processes_df %>% na.omit()

# gg_miss_var(processes_df)

processes_df$date <- as.Date(processes_df$date)

proc_df_summary <- processes_df %>%
  group_by(date) %>%
  summarise_all(list(mean))

###

library(DataExplorer)

introduce(proc_df_summary)
plot_histogram(proc_df_summary)
create_report(proc_df_summary)

###

proc_values <- proc_df_summary %>% select(-date)

preProcValues <- preProcess(proc_values, method = c("center", "scale"))

proc_vars_transformed <- predict(preProcValues, proc_values) %>%
  data.frame()

M <- cor(proc_vars_transformed)
corrplot.mixed(M)

###...###...###...###

nzv <- nearZeroVar(proc_values, freqCut = 95/5)

descrCor <- cor(proc_values)
summary(descrCor[upper.tri(descrCor)])
highlyCorDescr <- findCorrelation(descrCor, cutoff = .85)
filteredDescr <- proc_values[,-highlyCorDescr]

preProcValues <- preProcess(filteredDescr, method = c("center", "scale"))
filteredDescrs_transformed <- predict(preProcValues, filteredDescr) %>%
  data.frame()

filteredDF <- cbind(proc_df_summary$date, filteredDescrs_transformed)
filteredDF$Date <- filteredDF$'proc_df_summary$date'
filteredDF <- filteredDF %>% select(-`proc_df_summary$date`)

qaz <- left_join(df, filteredDF, by= 'Date') %>%
  na.omit()

## Regression:
set.seed(350)
qaz_values <- qaz %>%
  select(-Date)

qaz.rf <- randomForest(avgPS ~ ., data=qaz_values, mtry=3,
                       importance=TRUE, na.action=na.omit)
print(qaz.rf)
## Show "importance" of variables: higher value mean more important:
round(importance(qaz.rf), 2)

varImpPlot(qaz.rf)

